﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caldwell_Alexis_DBSReview
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Alexis Caldwell
             * 1-Oct-2017
             * Project and Portfolio II
             * DBS Review
                */
            //connect to database
            SqlConnection DBSReview = new SqlConnection("user id=root;" +"password=root;" + "server=98.168.188.1:8889;"+"Trusted_Connection=yes;" +  "database=SampleAPIData; " + "connection timeout=30");

            //open the connection
            try
            {
                DBSReview.Open();
            }
            catch (Exception)
            {
                Console.WriteLine("The connection failed.");
            }


            //city variable
            string city;

            //ask user which city they'd like to see
            Console.WriteLine("Which city would you like to view the weather?");
            city = Console.ReadLine();

            //make sure the user said a city
            while (string.IsNullOrWhiteSpace(city))
            {
                Console.WriteLine("Which city would you like to view the weather?");
                city = Console.ReadLine();
            }

            // read the selected city's weather and display it to the user
            try
            {
                SqlDataReader cityWeather = null;
                SqlCommand cityPrompt = new SqlCommand("SELECT temp, pressure, humidity, MAX(createdDate) FROM weather WHERE Column = " + city, DBSReview);
                cityWeather = cityPrompt.ExecuteReader();
                while (cityWeather.Read())
                {
                    Console.WriteLine("For {0}, the temperature is {1}, the pressure is {2}, and the humidity is {3}.", city, cityWeather["temp"].ToString(), cityWeather["pressure"].ToString(), cityWeather["humidity"].ToString());
                }
            }
            //if the city doesn't exist
            catch (Exception)
            {
                Console.WriteLine("No data availiable for {0}.",city);
            }


            // close the connection
            try
            {
                DBSReview.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Could not close connection");
            }

            //end 
        }
    }
}
